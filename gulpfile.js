var gulp = require('gulp');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');

gulp.task('browserify', function () {
	return browserify('./cliente/index.js')
			.bundle()
			.pipe(source('app.js'))
			.pipe(buffer())
			.pipe(uglify())
			.pipe(gulp.dest('./public/js'))
});

gulp.task('watch', function (){
	gulp.watch('./cliente/index.js', ['browserify']); 
});

gulp.task('default', ['watch']);